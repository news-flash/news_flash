DROP TRIGGER IF EXISTS on_delete_article_trigger;

ALTER TABLE enclosures RENAME TO _enclosures_old;

CREATE TABLE enclosures (
	article_id TEXT PRIMARY KEY NOT NULL REFERENCES articles(article_id),
	url TEXT NOT NULL,
	mime_type TEXT,
    title Text
);

INSERT INTO enclosures (article_id, url, mime_type, title)
  SELECT article_id, url, mime_type, title
  FROM _enclosures_old;


DROP TABLE _enclosures_old;

CREATE TRIGGER on_delete_article_trigger
	BEFORE DELETE ON articles
	BEGIN
		DELETE FROM taggings WHERE taggings.article_id=OLD.article_id;
		DELETE FROM enclosures WHERE enclosures.article_id=OLD.article_id;
		DELETE FROM thumbnails WHERE thumbnails.article_id=OLD.article_id;
	END;