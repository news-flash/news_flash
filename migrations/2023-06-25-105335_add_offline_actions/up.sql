CREATE TABLE offline_actions (
	action_type INTEGER NOT NULL,
	article_id TEXT NOT NULL,
	tag_id TEXT,
	PRIMARY KEY (action_type, article_id, tag_id)
);