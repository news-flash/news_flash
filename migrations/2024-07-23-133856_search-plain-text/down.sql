DROP TABLE fts_table;

CREATE VIRTUAL TABLE IF NOT EXISTS fts_table USING fts4 (content='articles', article_id, summary, title, author);

INSERT INTO fts_table(fts_table) VALUES('rebuild');

DROP INDEX article_unread_feeds;
DROP INDEX article_marked_feeds;