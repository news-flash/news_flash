DROP TABLE fts_table;

CREATE VIRTUAL TABLE IF NOT EXISTS fts_table USING fts4 (content='articles', article_id, summary, plain_text, title, author);

INSERT INTO fts_table(fts_table) VALUES('rebuild');

CREATE INDEX article_unread_feeds ON articles (feed_id, unread);
CREATE INDEX article_marked_feeds ON articles (feed_id, marked);