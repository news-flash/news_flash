use crate::feed_api::{FeedApiError, FeedApiResult};
use crate::models::FeedID;
use log::{error, info};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::Read;
use std::path::{Path, PathBuf};

static FILE_NAME: &str = "local_rss_cache.json";

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LocalRSSCache {
    #[serde(default)]
    etags: HashMap<FeedID, String>,
    #[serde(skip_serializing)]
    #[serde(skip_deserializing)]
    path: PathBuf,
}

impl LocalRSSCache {
    pub fn load(path: &Path) -> FeedApiResult<Self> {
        let path = path.join(FILE_NAME);
        if path.as_path().exists() {
            let mut contents = String::new();
            info!("Attempting to open local cache file: {:?}", path);
            let mut file = File::open(&path).map_err(move |err| {
                error!("Failed to load local cache file");
                FeedApiError::IO(err)
            })?;
            file.read_to_string(&mut contents).map_err(move |err| {
                error!("Reading content of file failed");
                FeedApiError::IO(err)
            })?;
            let mut cache: LocalRSSCache = serde_json::from_str(&contents)?;
            cache.path = path;

            return Ok(cache);
        }

        info!("Local cache file does not exist. Returning empty cache: {:?}", path);
        Ok(LocalRSSCache { etags: HashMap::new(), path })
    }

    pub fn get(&self, feed_id: &FeedID) -> Option<String> {
        self.etags.get(feed_id).cloned()
    }

    pub fn set(&mut self, feed_id: &FeedID, etag: Option<String>) {
        if let Some(etag) = etag {
            self.etags.insert(feed_id.clone(), etag);
        } else {
            self.etags.remove(feed_id);
        }
    }

    pub fn write(&self) -> FeedApiResult<()> {
        let data = serde_json::to_string_pretty(self)?;
        fs::write(&self.path, data).map_err(|err| {
            error!("Failed to write local cache file to: {:?}", self.path);
            FeedApiError::IO(err)
        })?;
        Ok(())
    }

    pub fn delete(&self) -> FeedApiResult<()> {
        fs::remove_file(&self.path)?;
        Ok(())
    }
}
