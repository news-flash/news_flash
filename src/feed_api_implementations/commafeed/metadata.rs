use super::config::AccountConfig;
use super::Commafeed;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiError, FeedApiResult, Portal};
use crate::models::{
    ApiSecret, DirectLoginGUI, LoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon,
};
use commafeed_api::CommafeedApi;
use rust_embed::RustEmbed;
use std::path::Path;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/commafeed/icons"]
struct CommafeedResources;

pub struct CommafeedMetadata;

impl CommafeedMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("commafeed")
    }
}

impl ApiMetadata for CommafeedMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = CommafeedResources::get("feed-service-commafeed.svg").ok_or(FeedApiError::Resource)?;
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = CommafeedResources::get("feed-service-commafeed-symbolic.svg").ok_or(FeedApiError::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Direct(DirectLoginGUI::default());

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Commafeed"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            // It's impossible for this to fail to parse so the unwrap is safe
            website: Some(Url::parse("https://feedbin.com/").unwrap()),
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::ApacheV2,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn get_instance(&self, path: &Path, portal: Box<dyn Portal>, _user_api_secret: Option<ApiSecret>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path)?;

        let mut api: Option<CommafeedApi> = None;

        if let (Some(url), Some(username), Some(password)) = (account_config.get_url(), account_config.get_user_name(), account_config.get_password())
        {
            if let Ok(url) = Url::parse(&url) {
                api = Some(CommafeedApi::new(&url, &username, &password));
            }
        }

        let commafeed = Commafeed {
            api,
            portal,
            config: account_config,
        };
        let commafeed = Box::new(commafeed);
        Ok(commafeed)
    }
}
