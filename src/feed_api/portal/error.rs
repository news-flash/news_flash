use thiserror::Error;

use crate::database::DatabaseError;

#[derive(Error, Debug)]
pub enum PortalError {
    #[error("Error reading from db")]
    DB(#[from] DatabaseError),
    #[error("Unknown Error")]
    Unknown,
}
