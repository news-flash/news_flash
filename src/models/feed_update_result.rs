use crate::models::{Enclosure, FatArticle, Feed, Marked, Read, Tagging};
use chrono::{Duration, Utc};

#[derive(Debug)]
pub struct FeedUpdateResult {
    pub feed: Option<Feed>,
    pub articles: Option<Vec<FatArticle>>,
    pub enclosures: Option<Vec<Enclosure>>,
    pub taggings: Option<Vec<Tagging>>,
}

impl FeedUpdateResult {
    pub fn remove_old_articles(self, older_than: Option<Duration>) -> Self {
        if let Some(older_than) = older_than {
            Self {
                feed: self.feed,
                articles: self.articles.map(|articles| {
                    articles
                        .into_iter()
                        .filter(|a| a.synced >= Utc::now() - older_than || a.unread == Read::Unread || a.marked == Marked::Marked)
                        .collect()
                }),
                enclosures: self.enclosures,
                taggings: self.taggings,
            }
        } else {
            self
        }
    }
}
