use crate::models::{ArticleID, TagID};
use crate::schema::{taggings, tags};

#[derive(Clone, Identifiable, Insertable, Queryable, PartialEq, Eq, Hash, Debug)]
#[diesel(primary_key(tag_id))]
#[diesel(table_name = tags)]
pub struct Tag {
    pub tag_id: TagID,
    pub label: String,
    pub color: Option<String>,
    pub sort_index: Option<i32>,
}

#[derive(Clone, Identifiable, Insertable, Queryable, Hash, PartialEq, Eq, Debug)]
#[diesel(primary_key(article_id))]
#[diesel(table_name = taggings)]
pub struct Tagging {
    pub article_id: ArticleID,
    pub tag_id: TagID,
}
