use thiserror::Error;
use tokio::sync::AcquireError;

#[derive(Error, Debug)]
pub enum FeedParserError {
    #[error("Http: {0}")]
    Http(#[from] reqwest::Error),
    #[error("Failed to parse feed url from HTML")]
    Html,
    #[error("Failed to parse feed")]
    Feed,
    #[error("No URL")]
    NoUrl,
    #[error("Couldn't acquire download permit from semaphore")]
    Semaphore(#[from] AcquireError),
}
